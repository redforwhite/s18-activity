console.log("Hello Madlang Pipol");


	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.

function sum(Number1, Number2){

	console.log("Displayed sum of " + Number1 + " and " + Number2);
	console.log(Number1 + Number2);
}

sum(5, 15);

	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

function difference(Number3, Number4){

	console.log("Displayed difference of " + Number3 + " and " + Number4);
	console.log(Number3 - Number4);
}

difference(20, 5);

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

function product(Number5, Number6){

	console.log("Displayed product of " + Number5 + " and " + Number6);
	console.log(Number5 * Number6);

	return	Number5 * Number6;

}


product(50, 10);


	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.


function quotient(Number7, Number8){

	console.log("Displayed quotient of " + Number7 + " and " + Number8);
	console.log(Number7 / Number8);

	return	Number7 / Number8;

}

quotient(50, 10);

	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

function circleArea(radius){

	let pi = 3.1415926535897932384626433;
	console.log("The result of getting the area of a circle with " + radius + " radius");
	console.log(pi * radius ** 2);

	return	pi * radius * radius;

}

circleArea(15);

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.

	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

function averageVar(one, two, three, four){

	console.log("The average of " + one + " " + two + " " + three + " " + four);
	console.log((one + two + three + four) / 4);

	return (one + two + three + four) / 4;

}

averageVar(20, 40, 60, 80);


	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.
	

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.


function isPassingScore(score, total){

	let passingGrade = total * 0.51
	console.log("Is " + score + "/" + total + " a passing score?");
	console.log(score >= passingGrade);
	
	return score >= passingGrade;

}

isPassingScore(38, 50)

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.
